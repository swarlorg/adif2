/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.brack11.adif2.utils;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * Created by Yury Bondarenko on 2017-04-12.
 * <p>
 * Owner: Yury Bondarenko
 * Website: http://www.swarl.org
 * Email: this.brack@gmail.com
 */
public class DateTimeWrapper {
    private static String datePattern = "yyyyMMdd";
    private static String timePattern = "HHmm";

    public static LocalDate ofDate(String strDate) {
        if (strDate.charAt(4) != '-') {
            return LocalDate.parse(strDate, DateTimeFormatter.ofPattern(datePattern));
        } else {
            return LocalDate.parse(strDate);
        }
    }

    public static LocalTime ofTime(String strTime) {
        if (strTime.charAt(2) != ':') {
            return LocalTime.parse(strTime, DateTimeFormatter.ofPattern(timePattern));
        } else {
            return LocalTime.parse(strTime);
        }
    }

    public static String toAdifDate(LocalDate date) {
        String adifDate = date.format(DateTimeFormatter.ofPattern(datePattern));
        return adifDate;
    }

    public static String toAdifTime(LocalTime time) {
        String adifTime = time.format(DateTimeFormatter.ofPattern(timePattern));
        return adifTime;
    }

    public static String nowUtcTime() {
        LocalTime utc = LocalTime.now(ZoneId.of("UTC"));
        return utc.format(DateTimeFormatter.ofPattern("HH:mm"));
    }
}
