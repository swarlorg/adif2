/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.brack11.adif2;

import javafx.collections.ObservableList;
import org.swarl.brack11.adif2.model.Adif2Fields;
import org.swarl.brack11.adif2.model.Adif2Record;
import org.swarl.brack11.adif2.utils.StringUtils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;

/**
 * Class is the part of adif parser package
 * Created by Yury Bondarenko on 3/16/17.
 */
public class Adif2Reader {

    private String filePath;
    private ObservableList<Adif2Record> records;

    /**
     * Constructor taking file path and ObservableList of records as parameters
     * and setting corresponding private variables
     *
     * @param file
     * @param records
     */
    public Adif2Reader(String file, ObservableList<Adif2Record> records) {
        this.records = records;
        filePath = file;
    }

    /**
     * Records getter
     * @return
     */
    public ObservableList<Adif2Record> getRecords() {
        return records;
    }

    /**
     * Read through adif file
     *
     * @throws IOException
     */
    public void read() throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(filePath));

        int intValue;
        boolean createToken = false; // command to create token
        boolean createSize = false; // command to create size
        StringBuilder token = new StringBuilder(); // token container
        StringBuilder size = new StringBuilder(); // size container
        Adif2Record record = new Adif2Record();

        while ((intValue = br.read()) != -1) {
            switch (intValue) {
                case '\n':
                    break;
                case '<':
                    createToken = true;
                    break;
                case ':':
                    if (createToken) {
                        // not in header
                        createToken = false;
                        createSize = true;
                    } else if (createSize) {
                        createSize = false;
                    }
                    break;
                case '>':
                    switch (token.toString().toLowerCase()) {
                        case "eoh":
                            record = new Adif2Record();  // create new record
                            break;
                        case "eor":
                            records.add(record); // add formed Adif2Record record to ObservedList
                            record = new Adif2Record(); // create new record
                            break;
                        default:
                            createSize = false;
                            // get data for every token within a record
                            createData(br, token.toString(), StringUtils.str2int(size.toString()), record);
                    }
                    // empty token and size
                    token.setLength(0);
                    size.setLength(0);
                    break;
                default:
                    char cValue = (char) intValue;
                    if (createToken) {
                        token.append(cValue); // form token
                    }
                    if (createSize) {
                        size.append(cValue); // form size or the token
                    }
            }

        }

    }

    /**
     * Extracting data from bufferedreader which belongs to token with the given size
     * the data with token will be add to Adif2Record
     *
     * @param br
     * @param token
     * @param size
     * @param record
     * @throws IOException
     */
    private void createData(BufferedReader br, String token, int size, Adif2Record record) throws IOException {
        StringBuilder sb = new StringBuilder(size);
        for (int i = 0; i < size; i++) {
            int c = br.read();
            if (c == -1) {
                throw new IOException("Unexpected end of input");
            }
            sb.appendCodePoint((char) c);
        }
        String data = sb.toString().trim();
        if (data.length() > 0) {
            assignData(token.trim(), data, record);
        }
    }

    /**
     * Assign data to the given token
     * and record formed
     *
     * @param token
     * @param data
     * @param record
     */
    private void assignData(String token, String data, Adif2Record record) {
        for (Adif2Fields field : Adif2Fields.values()) {
            try {
                if ((field.name().equalsIgnoreCase(token)) && (!token.equalsIgnoreCase("PROGRAMID")) && (!token.equalsIgnoreCase("ADIF_Ver"))) {
                    String methodName = "set" + StringUtils.toCamelCase(token);
                    Method setNameMethod = record.getClass().getMethod(methodName, String.class);
                    setNameMethod.invoke(record, data);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

}
