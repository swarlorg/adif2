/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.brack11.adif2.model;

import javafx.beans.property.*;
import org.swarl.brack11.adif2.enums.Band;
import org.swarl.brack11.adif2.enums.Mode;
import org.swarl.brack11.adif2.enums.QSLRcvd;
import org.swarl.brack11.adif2.utils.DateTimeWrapper;
import org.swarl.brack11.location.model.Coordinate;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Class is the part of adif parser package
 * Created by Yury Bondarenko on 3/16/17.
 */
public class Adif2Record {

    private StringProperty call = new SimpleStringProperty();
    private StringProperty comment = new SimpleStringProperty();
    private ObjectProperty<Band> band = new SimpleObjectProperty<>();
    private ObjectProperty<LocalDate> qsoDate = new SimpleObjectProperty<>();
    private DoubleProperty freq = new SimpleDoubleProperty();
    private ObjectProperty<Mode> mode = new SimpleObjectProperty<>();
    private StringProperty rstRcvd = new SimpleStringProperty();
    private ObjectProperty<LocalTime> timeOn = new SimpleObjectProperty<>();

    private StringProperty address = new SimpleStringProperty();
    private StringProperty age = new SimpleStringProperty();
    private StringProperty aIndex = new SimpleStringProperty();
    private StringProperty antAz = new SimpleStringProperty();
    private StringProperty andEl = new SimpleStringProperty();
    private StringProperty antPath = new SimpleStringProperty();
    private StringProperty arrlSect = new SimpleStringProperty();
    private ObjectProperty<Band> bandRx = new SimpleObjectProperty<>();
    private StringProperty check = new SimpleStringProperty();
    private StringProperty clasS = new SimpleStringProperty();
    private StringProperty cnty = new SimpleStringProperty();
    private StringProperty cont = new SimpleStringProperty();
    private StringProperty contactedOp = new SimpleStringProperty();
    private StringProperty contestId = new SimpleStringProperty();
    private StringProperty country = new SimpleStringProperty();
    private StringProperty cqz = new SimpleStringProperty();
    private StringProperty creditSubmitted = new SimpleStringProperty();
    private StringProperty creditGranted = new SimpleStringProperty();
    private StringProperty distance = new SimpleStringProperty();
    private StringProperty dxcc = new SimpleStringProperty();
    private StringProperty email = new SimpleStringProperty();
    private StringProperty eqCall = new SimpleStringProperty();
    private ObjectProperty<LocalDate> eQslQslRDate = new SimpleObjectProperty<>();
    private ObjectProperty<LocalDate> eQslQslSDate = new SimpleObjectProperty<>();
    private ObjectProperty<QSLRcvd> eQslQslRcvd = new SimpleObjectProperty<>();
    private StringProperty eQslQslSent = new SimpleStringProperty();
    private StringProperty forceInit = new SimpleStringProperty();
    private DoubleProperty freqRx = new SimpleDoubleProperty();
    private StringProperty gridsquare = new SimpleStringProperty();
    private StringProperty guestOp = new SimpleStringProperty();
    private StringProperty iota = new SimpleStringProperty();
    private StringProperty iotaIslandId = new SimpleStringProperty();
    private StringProperty ituz = new SimpleStringProperty();
    private StringProperty kIndex = new SimpleStringProperty();
    private ObjectProperty<Coordinate> lat = new SimpleObjectProperty<>();
    private ObjectProperty<Coordinate> lon = new SimpleObjectProperty<>();
    private ObjectProperty<LocalDate> lotwQslRDate = new SimpleObjectProperty<>();
    private ObjectProperty<LocalDate> lotwQslSDate = new SimpleObjectProperty<>();
    private ObjectProperty<QSLRcvd> lotwQslRcvd = new SimpleObjectProperty<>();
    private StringProperty lotwQslSent = new SimpleStringProperty();
    private StringProperty maxBursts = new SimpleStringProperty();
    private StringProperty msShower = new SimpleStringProperty();
    private StringProperty myCity = new SimpleStringProperty();
    private StringProperty myCnty = new SimpleStringProperty();
    private StringProperty myCountry = new SimpleStringProperty();
    private StringProperty myCqZone = new SimpleStringProperty();
    private StringProperty myGridSquare = new SimpleStringProperty();
    private StringProperty myIota = new SimpleStringProperty();
    private StringProperty myIotaIslandId = new SimpleStringProperty();
    private StringProperty myItuZone = new SimpleStringProperty();
    private ObjectProperty<Coordinate> myLat = new SimpleObjectProperty<>();
    private ObjectProperty<Coordinate> myLon = new SimpleObjectProperty<>();
    private StringProperty myName = new SimpleStringProperty();
    private StringProperty myPostalCode = new SimpleStringProperty();
    private StringProperty myRig = new SimpleStringProperty();
    private StringProperty mySig = new SimpleStringProperty();
    private StringProperty mySigInfo = new SimpleStringProperty();
    private StringProperty myState = new SimpleStringProperty();
    private StringProperty myStreet = new SimpleStringProperty();
    private StringProperty name = new SimpleStringProperty();
    private StringProperty notes = new SimpleStringProperty();
    private StringProperty nrBursts = new SimpleStringProperty();
    private StringProperty operator = new SimpleStringProperty();
    private StringProperty ownerCallSign = new SimpleStringProperty();
    private StringProperty pfx = new SimpleStringProperty();
    private StringProperty precedence = new SimpleStringProperty();
    private StringProperty programVersion = new SimpleStringProperty();
    private StringProperty propMode = new SimpleStringProperty();
    private StringProperty publicKey = new SimpleStringProperty();
    private StringProperty qslmsg = new SimpleStringProperty();
    private ObjectProperty<LocalDate> qslRDate = new SimpleObjectProperty<>();
    private ObjectProperty<LocalDate> qslSDate = new SimpleObjectProperty<>();
    private ObjectProperty<QSLRcvd> qslRcvd = new SimpleObjectProperty<>();
    private StringProperty qslRcvdVia = new SimpleStringProperty();
    private StringProperty qslSent = new SimpleStringProperty();
    private StringProperty qslSentVia = new SimpleStringProperty();
    private StringProperty qslVia = new SimpleStringProperty();
    private StringProperty qsoComplete = new SimpleStringProperty();
    private ObjectProperty<LocalDate> qsoDateOff = new SimpleObjectProperty<>();
    private StringProperty qsoRandom = new SimpleStringProperty();
    private StringProperty qth = new SimpleStringProperty();
    private StringProperty rig = new SimpleStringProperty();
    private StringProperty rstSent = new SimpleStringProperty();
    private StringProperty rxPwr = new SimpleStringProperty();
    private StringProperty satMode = new SimpleStringProperty();
    private StringProperty satName = new SimpleStringProperty();
    private StringProperty sfi = new SimpleStringProperty();
    private StringProperty sig = new SimpleStringProperty();
    private StringProperty sigInfo = new SimpleStringProperty();
    private StringProperty sRx = new SimpleStringProperty();
    private StringProperty sRxString = new SimpleStringProperty();
    private StringProperty state = new SimpleStringProperty();
    private StringProperty stationCallSign = new SimpleStringProperty();
    private StringProperty sTx = new SimpleStringProperty();
    private StringProperty sTxString = new SimpleStringProperty();
    private StringProperty swl = new SimpleStringProperty();
    private StringProperty tenTen = new SimpleStringProperty();
    private ObjectProperty<LocalTime> timeOff = new SimpleObjectProperty<>();
    private StringProperty txPwr = new SimpleStringProperty();
    private StringProperty veProv = new SimpleStringProperty();
    private StringProperty web = new SimpleStringProperty();
    private StringProperty appSwarlogTerritory = new SimpleStringProperty();
    private StringProperty appSwarlogTz = new SimpleStringProperty();

    private StringProperty custom1 = new SimpleStringProperty();
    private StringProperty custom2 = new SimpleStringProperty();
    private StringProperty custom3 = new SimpleStringProperty();
    private StringProperty custom4 = new SimpleStringProperty();


    public Adif2Record() {
    }

    public String getCall() {
        return call.get();
    }

    public StringProperty callProperty() {
        return call;
    }

    public void setCall(String call) {
        this.call.set(call.toUpperCase());
    }

    public String getComment() {
        return comment.get();
    }

    public StringProperty commentProperty() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment.set(comment.toUpperCase());
    }

    public Band getBand() {
        return band.get();
    }

    public ObjectProperty<Band> bandProperty() {
        return band;
    }

    public void setBand(String band) {
        Band b = Band.getByName(band);
        setBand(b);
    }

    public void setBand(Band band) {
        this.band.set(band);
    }

    public LocalDate getQsoDate() {
        return qsoDate.get();
    }

    public ObjectProperty<LocalDate> qsoDateProperty() {
        return qsoDate;
    }

    public void setQsoDate(LocalDate qsoDate) {
        this.qsoDate.set(qsoDate);
    }

    public void setQsoDate(String qsoDate) {
        this.qsoDate.set(DateTimeWrapper.ofDate(qsoDate));
    }

    public Double getFreq() {
        return freq.get();
    }

    public DoubleProperty freqProperty() {
        return freq;
    }

    public void setFreq(Double freq) {
        double f = Band.kHzTomHz(freq);
        this.freq.set(f);
    }

    public void setFreq(String freq) {
        Double f = Double.parseDouble(freq);
        setFreq(f);
    }

    public Mode getMode() {
        return mode.get();
    }

    public ObjectProperty<Mode> modeProperty() {
        return mode;
    }

    public void setMode(Mode mode) {
        this.mode.set(mode);
    }

    public void setMode(String mode) {
        Mode m = Mode.valueOf(mode.toUpperCase());
        this.mode.set(m);
    }
    public String getRstRcvd() {
        return rstRcvd.get();
    }

    public StringProperty rstRcvdProperty() {
        return rstRcvd;
    }

    public void setRstRcvd(String rstRcvd) {
        this.rstRcvd.set(rstRcvd);
    }

    public LocalTime getTimeOn() {
        return timeOn.get();
    }

    public ObjectProperty<LocalTime> timeOnProperty() {
        return timeOn;
    }

    public void setTimeOn(LocalTime timeOn) {
        this.timeOn.set(timeOn);
    }

    public void setTimeOn(String timeOn) {
        this.timeOn.set(DateTimeWrapper.ofTime(timeOn));
    }

    public String getAddress() {
        return address.get();
    }

    public StringProperty addressProperty() {
        return address;
    }

    public void setAddress(String address) {
        this.address.set(address.toUpperCase());
    }

    public String getAge() {
        return age.get();
    }

    public StringProperty ageProperty() {
        return age;
    }

    public void setAge(String age) {
        this.age.set(age);
    }

    public String getaIndex() {
        return aIndex.get();
    }

    public StringProperty aIndexProperty() {
        return aIndex;
    }

    public void setaIndex(String aIndex) {
        this.aIndex.set(aIndex);
    }

    public String getAntAz() {
        return antAz.get();
    }

    public StringProperty antAzProperty() {
        return antAz;
    }

    public void setAntAz(String antAz) {
        this.antAz.set(antAz);
    }

    public String getAndEl() {
        return andEl.get();
    }

    public StringProperty andElProperty() {
        return andEl;
    }

    public void setAndEl(String andEl) {
        this.andEl.set(andEl);
    }

    public String getAntPath() {
        return antPath.get();
    }

    public StringProperty antPathProperty() {
        return antPath;
    }

    public void setAntPath(String antPath) {
        this.antPath.set(antPath);
    }

    public String getArrlSect() {
        return arrlSect.get();
    }

    public StringProperty arrlSectProperty() {
        return arrlSect;
    }

    public void setArrlSect(String arrlSect) {
        this.arrlSect.set(arrlSect);
    }

    public Band getBandRx() {
        return bandRx.get();
    }

    public ObjectProperty<Band> bandRxProperty() {
        return bandRx;
    }

    public void setBandRx(Band bandRx) {
        this.bandRx.set(bandRx);
    }

    public void setBandRx(String bandRx) {
        Band b = Band.getByName(bandRx.toUpperCase());
        setBandRx(b);
    }

    public String getCheck() {
        return check.get();
    }

    public StringProperty checkProperty() {
        return check;
    }

    public void setCheck(String check) {
        this.check.set(check);
    }

    public String getClasS() {
        return clasS.get();
    }

    public StringProperty clasSProperty() {
        return clasS;
    }

    public void setClasS(String clasS) {
        this.clasS.set(clasS);
    }

    public String getCnty() {
        return cnty.get();
    }

    public StringProperty cntyProperty() {
        return cnty;
    }

    public void setCnty(String cnty) {
        this.cnty.set(cnty);
    }

    public String getCont() {
        return cont.get();
    }

    public StringProperty contProperty() {
        return cont;
    }

    public void setCont(String cont) {
        this.cont.set(cont);
    }

    public String getContactedOp() {
        return contactedOp.get();
    }

    public StringProperty contactedOpProperty() {
        return contactedOp;
    }

    public void setContactedOp(String contactedOp) {
        this.contactedOp.set(contactedOp);
    }

    public String getContestId() {
        return contestId.get();
    }

    public StringProperty contestIdProperty() {
        return contestId;
    }

    public void setContestId(String contestId) {
        this.contestId.set(contestId);
    }

    public String getCountry() {
        return country.get();
    }

    public StringProperty countryProperty() {
        return country;
    }

    public void setCountry(String country) {
        this.country.set(country.toUpperCase());
    }

    public String getCqz() {
        return cqz.get();
    }

    public StringProperty cqzProperty() {
        return cqz;
    }

    public void setCqz(String cqz) {
        this.cqz.set(cqz);
    }

    public String getCreditSubmitted() {
        return creditSubmitted.get();
    }

    public StringProperty creditSubmittedProperty() {
        return creditSubmitted;
    }

    public void setCreditSubmitted(String creditSubmitted) {
        this.creditSubmitted.set(creditSubmitted);
    }

    public String getCreditGranted() {
        return creditGranted.get();
    }

    public StringProperty creditGrantedProperty() {
        return creditGranted;
    }

    public void setCreditGranted(String creditGranted) {
        this.creditGranted.set(creditGranted);
    }

    public String getDistance() {
        return distance.get();
    }

    public StringProperty distanceProperty() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance.set(distance);
    }

    public String getDxcc() {
        return dxcc.get();
    }

    public StringProperty dxccProperty() {
        return dxcc;
    }

    public void setDxcc(String dxcc) {
        this.dxcc.set(dxcc);
    }

    public String getEmail() {
        return email.get();
    }

    public StringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email.toUpperCase());
    }

    public String getEqCall() {
        return eqCall.get();
    }

    public StringProperty eqCallProperty() {
        return eqCall;
    }

    public void setEqCall(String eqCall) {
        this.eqCall.set(eqCall.toUpperCase());
    }

    public LocalDate getEqslQslRDate() {
        return eQslQslRDate.get();
    }

    public ObjectProperty<LocalDate> eQslQslRDateProperty() {
        return eQslQslRDate;
    }

    public void setEqslQslRDate(LocalDate eQslQslRDate) {
        this.eQslQslRDate.set(eQslQslRDate);
    }

    public void setEqslQslRDate(String eQslQslRDate) {
        this.eQslQslRDate.set(DateTimeWrapper.ofDate(eQslQslRDate));
    }

    public LocalDate getEqslQslSDate() {
        return eQslQslSDate.get();
    }

    public ObjectProperty<LocalDate> eQslQslSDateProperty() {
        return eQslQslSDate;
    }

    public void setEqslQslSDate(LocalDate eQslQslSDate) {
        this.eQslQslSDate.set(eQslQslSDate);
    }

    public void setEqslQslSDate(String eQslQslSDate) {
        this.eQslQslSDate.set(DateTimeWrapper.ofDate(eQslQslSDate));
    }

    public QSLRcvd getEqslQslRcvd() {
        return eQslQslRcvd.get();
    }

    public ObjectProperty<QSLRcvd> eQslQslRcvdProperty() {
        return eQslQslRcvd;
    }

    public void setEqslQslRcvd(QSLRcvd eQslQslRcvd) {
        this.eQslQslRcvd.set(eQslQslRcvd);
    }

    public void setEqslQslRcvd(String eQslQslRcvd) {
        QSLRcvd q = QSLRcvd.getByCode(eQslQslRcvd.charAt(0));
        this.eQslQslRcvd.set(q);
    }

    public String getEqslQslSent() {
        return eQslQslSent.get();
    }

    public StringProperty eQslQslSentProperty() {
        return eQslQslSent;
    }

    public void setEqslQslSent(String eQslQslSent) {
        this.eQslQslSent.set(eQslQslSent);
    }

    public String getForceInit() {
        return forceInit.get();
    }

    public StringProperty forceInitProperty() {
        return forceInit;
    }

    public void setForceInit(String forceInit) {
        this.forceInit.set(forceInit);
    }

    public Double getFreqRx() {
        return freqRx.get();
    }

    public DoubleProperty freqRxProperty() {
        return freqRx;
    }

    public void setFreqRx(Double freqRx) {
        double f = Band.kHzTomHz(freqRx);
        this.freqRx.set(f);
    }

    public void setFreqRx(String freqRx) {
        Double f = Double.parseDouble(freqRx);
        setFreqRx(f);
    }

    public String getGridsquare() {
        return gridsquare.get();
    }

    public StringProperty gridsquareProperty() {
        return gridsquare;
    }

    public void setGridsquare(String gridsquare) {
        this.gridsquare.set(gridsquare);
    }

    public String getGuestOp() {
        return guestOp.get();
    }

    public StringProperty guestOpProperty() {
        return guestOp;
    }

    public void setGuestOp(String guestOp) {
        this.guestOp.set(guestOp);
    }

    public String getIota() {
        return iota.get();
    }

    public StringProperty iotaProperty() {
        return iota;
    }

    public void setIota(String iota) {
        this.iota.set(iota);
    }

    public String getIotaIslandId() {
        return iotaIslandId.get();
    }

    public StringProperty iotaIslandIdProperty() {
        return iotaIslandId;
    }

    public void setIotaIslandId(String iotaIslandId) {
        this.iotaIslandId.set(iotaIslandId);
    }

    public String getItuz() {
        return ituz.get();
    }

    public StringProperty ituzProperty() {
        return ituz;
    }

    public void setItuz(String ituz) {
        this.ituz.set(ituz);
    }

    public String getkIndex() {
        return kIndex.get();
    }

    public StringProperty kIndexProperty() {
        return kIndex;
    }

    public void setkIndex(String kIndex) {
        this.kIndex.set(kIndex);
    }

    public Coordinate getLat() {
        return lat.get();
    }

    public ObjectProperty<Coordinate> latProperty() {
        return lat;
    }

    public void setLat(Coordinate lat) {
        this.lat.set(lat);
    }

    public void setLat(String lat) {
        Coordinate c = new Coordinate("lat", lat);
        this.lat.set(c);
    }

    public Coordinate getLon() {
        return lon.get();
    }

    public ObjectProperty<Coordinate> lonProperty() {
        return lon;
    }

    public void setLon(Coordinate lon) {
        this.lon.set(lon);
    }

    public void setLon(String lon) {
        Coordinate c = new Coordinate("lon", lon);
        this.lon.set(c);
    }

    public LocalDate getLotwQslRDate() {
        return lotwQslRDate.get();
    }

    public ObjectProperty<LocalDate> lotwQslRDateProperty() {
        return lotwQslRDate;
    }

    public void setLotwQslRDate(LocalDate lotwQslRDate) {
        this.lotwQslRDate.set(lotwQslRDate);
    }

    public void setLotwQslRDate(String lotwQslRDate) {
        this.lotwQslRDate.set(DateTimeWrapper.ofDate(lotwQslRDate));
    }

    public LocalDate getLotwQslSDate() {
        return lotwQslSDate.get();
    }

    public ObjectProperty<LocalDate> lotwQslSDateProperty() {
        return lotwQslSDate;
    }

    public void setLotwQslSDate(LocalDate lotwQslSDate) {
        this.lotwQslSDate.set(lotwQslSDate);
    }

    public void setLotwQslSDate(String lotwQslSDate) {
        this.lotwQslSDate.set(DateTimeWrapper.ofDate(lotwQslSDate));
    }

    public QSLRcvd getLotwQslRcvd() {
        return lotwQslRcvd.get();
    }

    public ObjectProperty<QSLRcvd> lotwQslRcvdProperty() {
        return lotwQslRcvd;
    }

    public void setLotwQslRcvd(QSLRcvd lotwQslRcvd) {
        this.lotwQslRcvd.set(lotwQslRcvd);
    }

    public void setLotwQslRcvd(String lotwQslRcvd) {
        QSLRcvd q = QSLRcvd.getByCode(lotwQslRcvd.charAt(0));
        this.lotwQslRcvd.set(q);
    }

    public String getLotwQslSent() {
        return lotwQslSent.get();
    }

    public StringProperty lotwQslSentProperty() {
        return lotwQslSent;
    }

    public void setLotwQslSent(String lotwQslSent) {
        this.lotwQslSent.set(lotwQslSent);
    }

    public String getMaxBursts() {
        return maxBursts.get();
    }

    public StringProperty maxBurstsProperty() {
        return maxBursts;
    }

    public void setMaxBursts(String maxBursts) {
        this.maxBursts.set(maxBursts);
    }

    public String getMsShower() {
        return msShower.get();
    }

    public StringProperty msShowerProperty() {
        return msShower;
    }

    public void setMsShower(String msShower) {
        this.msShower.set(msShower);
    }

    public String getMyCity() {
        return myCity.get();
    }

    public StringProperty myCityProperty() {
        return myCity;
    }

    public void setMyCity(String myCity) {
        this.myCity.set(myCity.toUpperCase());
    }

    public String getMyCnty() {
        return myCnty.get();
    }

    public StringProperty myCntyProperty() {
        return myCnty;
    }

    public void setMyCnty(String myCnty) {
        this.myCnty.set(myCnty);
    }

    public String getMyCountry() {
        return myCountry.get();
    }

    public StringProperty myCountryProperty() {
        return myCountry;
    }

    public void setMyCountry(String myCountry) {
        this.myCountry.set(myCountry.toUpperCase());
    }

    public String getMyCqZone() {
        return myCqZone.get();
    }

    public StringProperty myCqZoneProperty() {
        return myCqZone;
    }

    public void setMyCqZone(String myCqZone) {
        this.myCqZone.set(myCqZone);
    }

    public String getMyGridSquare() {
        return myGridSquare.get();
    }

    public StringProperty myGridSquareProperty() {
        return myGridSquare;
    }

    public void setMyGridSquare(String myGridSquare) {
        this.myGridSquare.set(myGridSquare.toUpperCase());
    }

    public String getMyIota() {
        return myIota.get();
    }

    public StringProperty myIotaProperty() {
        return myIota;
    }

    public void setMyIota(String myIota) {
        this.myIota.set(myIota.toUpperCase());
    }

    public String getMyIotaIslandId() {
        return myIotaIslandId.get();
    }

    public StringProperty myIotaIslandIdProperty() {
        return myIotaIslandId;
    }

    public void setMyIotaIslandId(String myIotaIslandId) {
        this.myIotaIslandId.set(myIotaIslandId);
    }

    public String getMyItuZone() {
        return myItuZone.get();
    }

    public StringProperty myItuZoneProperty() {
        return myItuZone;
    }

    public void setMyItuZone(String myItuZone) {
        this.myItuZone.set(myItuZone);
    }

    public Coordinate getMyLat() {
        return myLat.get();
    }

    public ObjectProperty<Coordinate> myLatProperty() {
        return myLat;
    }

    public void setMyLat(Coordinate myLat) {
        this.myLat.set(myLat);
    }

    public void setMyLat(String myLat) {
        Coordinate c = new Coordinate("lat", myLat);
        this.myLat.set(c);
    }

    public Coordinate getMyLon() {
        return myLon.get();
    }

    public ObjectProperty<Coordinate> myLonProperty() {
        return myLon;
    }

    public void setMyLon(Coordinate myLon) {
        this.myLon.set(myLon);
    }

    public void setMyLon(String myLon) {
        Coordinate c = new Coordinate("lon", myLon);
        this.myLon.set(c);
    }

    public String getMyName() {
        return myName.get();
    }

    public StringProperty myNameProperty() {
        return myName;
    }

    public void setMyName(String myName) {
        this.myName.set(myName);
    }

    public String getMyPostalCode() {
        return myPostalCode.get();
    }

    public StringProperty myPostalCodeProperty() {
        return myPostalCode;
    }

    public void setMyPostalCode(String myPostalCode) {
        this.myPostalCode.set(myPostalCode);
    }

    public String getMyRig() {
        return myRig.get();
    }

    public StringProperty myRigProperty() {
        return myRig;
    }

    public void setMyRig(String myRig) {
        this.myRig.set(myRig);
    }

    public String getMySig() {
        return mySig.get();
    }

    public StringProperty mySigProperty() {
        return mySig;
    }

    public void setMySig(String mySig) {
        this.mySig.set(mySig);
    }

    public String getMySigInfo() {
        return mySigInfo.get();
    }

    public StringProperty mySigInfoProperty() {
        return mySigInfo;
    }

    public void setMySigInfo(String mySigInfo) {
        this.mySigInfo.set(mySigInfo);
    }

    public String getMyState() {
        return myState.get();
    }

    public StringProperty myStateProperty() {
        return myState;
    }

    public void setMyState(String myState) {
        this.myState.set(myState);
    }

    public String getMyStreet() {
        return myStreet.get();
    }

    public StringProperty myStreetProperty() {
        return myStreet;
    }

    public void setMyStreet(String myStreet) {
        this.myStreet.set(myStreet);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getNotes() {
        return notes.get();
    }

    public StringProperty notesProperty() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes.set(notes);
    }

    public String getNrBursts() {
        return nrBursts.get();
    }

    public StringProperty nrBurstsProperty() {
        return nrBursts;
    }

    public void setNrBursts(String nrBursts) {
        this.nrBursts.set(nrBursts);
    }

    public String getOperator() {
        return operator.get();
    }

    public StringProperty operatorProperty() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator.set(operator);
    }

    public String getOwnerCallSign() {
        return ownerCallSign.get();
    }

    public StringProperty ownerCallSignProperty() {
        return ownerCallSign;
    }

    public void setOwnerCallSign(String ownerCallSign) {
        this.ownerCallSign.set(ownerCallSign);
    }

    public String getPfx() {
        return pfx.get();
    }

    public StringProperty pfxProperty() {
        return pfx;
    }

    public void setPfx(String pfx) {
        this.pfx.set(pfx);
    }

    public String getPrecedence() {
        return precedence.get();
    }

    public StringProperty precedenceProperty() {
        return precedence;
    }

    public void setPrecedence(String precedence) {
        this.precedence.set(precedence);
    }

    public String getProgramVersion() {
        return programVersion.get();
    }

    public StringProperty programVersionProperty() {
        return programVersion;
    }

    public void setProgramVersion(String programVersion) {
        this.programVersion.set(programVersion);
    }

    public String getPropMode() {
        return propMode.get();
    }

    public StringProperty propModeProperty() {
        return propMode;
    }

    public void setPropMode(String propMode) {
        this.propMode.set(propMode);
    }

    public String getPublicKey() {
        return publicKey.get();
    }

    public StringProperty publicKeyProperty() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey.set(publicKey);
    }

    public String getQslmsg() {
        return qslmsg.get();
    }

    public StringProperty qslmsgProperty() {
        return qslmsg;
    }

    public void setQslmsg(String qslmsg) {
        this.qslmsg.set(qslmsg.toUpperCase());
    }

    public LocalDate getQslRDate() {
        return qslRDate.get();
    }

    public ObjectProperty<LocalDate> qslRDateProperty() {
        return qslRDate;
    }

    public void setQslRDate(LocalDate qslRDate) {
        this.qslRDate.set(qslRDate);
    }

    public void setQslRDate(String qslRDate) {
        this.qslRDate.set(DateTimeWrapper.ofDate(qslRDate));
    }

    public LocalDate getQslSDate() {
        return qslSDate.get();
    }

    public ObjectProperty<LocalDate> qslSDateProperty() {
        return qslSDate;
    }

    public void setQslSDate(LocalDate qslSDate) {
        this.qslSDate.set(qslSDate);
    }

    public void setQslSDate(String qslSDate) {
        this.qslSDate.set(DateTimeWrapper.ofDate(qslSDate));
    }

    public QSLRcvd getQslRcvd() {
        return qslRcvd.get();
    }

    public ObjectProperty<QSLRcvd> qslRcvdProperty() {
        return qslRcvd;
    }

    public void setQslRcvd(QSLRcvd qslRcvd) {
        this.qslRcvd.set(qslRcvd);
    }

    public void setQslRcvd(String qslRcvd) {
        QSLRcvd q = QSLRcvd.getByCode(qslRcvd.charAt(0));
        this.qslRcvd.set(q);
    }

    public String getQslRcvdVia() {
        return qslRcvdVia.get();
    }

    public StringProperty qslRcvdViaProperty() {
        return qslRcvdVia;
    }

    public void setQslRcvdVia(String qslRcvdVia) {
        this.qslRcvdVia.set(qslRcvdVia);
    }

    public String getQslSent() {
        return qslSent.get();
    }

    public StringProperty qslSentProperty() {
        return qslSent;
    }

    public void setQslSent(String qslSent) {
        this.qslSent.set(qslSent);
    }

    public String getQslSentVia() {
        return qslSentVia.get();
    }

    public StringProperty qslSentViaProperty() {
        return qslSentVia;
    }

    public void setQslSentVia(String qslSentVia) {
        this.qslSentVia.set(qslSentVia);
    }

    public String getQslVia() {
        return qslVia.get();
    }

    public StringProperty qslViaProperty() {
        return qslVia;
    }

    public void setQslVia(String qslVia) {
        this.qslVia.set(qslVia);
    }

    public String getQsoComplete() {
        return qsoComplete.get();
    }

    public StringProperty qsoCompleteProperty() {
        return qsoComplete;
    }

    public void setQsoComplete(String qsoComplete) {
        this.qsoComplete.set(qsoComplete);
    }

    public LocalDate getQsoDateOff() {
        return qsoDateOff.get();
    }

    public ObjectProperty<LocalDate> qsoDateOffProperty() {
        return qsoDateOff;
    }

    public void setQsoDateOff(LocalDate qsoDateOff) {
        this.qsoDateOff.set(qsoDateOff);
    }

    public void setQsoDateOff(String qsoDateOff) {
        this.qsoDateOff.set(DateTimeWrapper.ofDate(qsoDateOff));
    }

    public String getQsoRandom() {
        return qsoRandom.get();
    }

    public StringProperty qsoRandomProperty() {
        return qsoRandom;
    }

    public void setQsoRandom(String qsoRandom) {
        this.qsoRandom.set(qsoRandom);
    }

    public String getQth() {
        return qth.get();
    }

    public StringProperty qthProperty() {
        return qth;
    }

    public void setQth(String qth) {
        this.qth.set(qth.toUpperCase());
    }

    public String getRig() {
        return rig.get();
    }

    public StringProperty rigProperty() {
        return rig;
    }

    public void setRig(String rig) {
        this.rig.set(rig.toUpperCase());
    }

    public String getRstSent() {
        return rstSent.get();
    }

    public StringProperty rstSentProperty() {
        return rstSent;
    }

    public void setRstSent(String rstSent) {
        this.rstSent.set(rstSent);
    }

    public String getRxPwr() {
        return rxPwr.get();
    }

    public StringProperty rxPwrProperty() {
        return rxPwr;
    }

    public void setRxPwr(String rxPwr) {
        this.rxPwr.set(rxPwr);
    }

    public String getSatMode() {
        return satMode.get();
    }

    public StringProperty satModeProperty() {
        return satMode;
    }

    public void setSatMode(String satMode) {
        this.satMode.set(satMode);
    }

    public String getSatName() {
        return satName.get();
    }

    public StringProperty satNameProperty() {
        return satName;
    }

    public void setSatName(String satName) {
        this.satName.set(satName.toUpperCase());
    }

    public String getSfi() {
        return sfi.get();
    }

    public StringProperty sfiProperty() {
        return sfi;
    }

    public void setSfi(String sfi) {
        this.sfi.set(sfi);
    }

    public String getSig() {
        return sig.get();
    }

    public StringProperty sigProperty() {
        return sig;
    }

    public void setSig(String sig) {
        this.sig.set(sig);
    }

    public String getSigInfo() {
        return sigInfo.get();
    }

    public StringProperty sigInfoProperty() {
        return sigInfo;
    }

    public void setSigInfo(String sigInfo) {
        this.sigInfo.set(sigInfo);
    }

    public String getsRx() {
        return sRx.get();
    }

    public StringProperty sRxProperty() {
        return sRx;
    }

    public void setsRx(String sRx) {
        this.sRx.set(sRx);
    }

    public String getsRxString() {
        return sRxString.get();
    }

    public StringProperty sRxStringProperty() {
        return sRxString;
    }

    public void setsRxString(String sRxString) {
        this.sRxString.set(sRxString);
    }

    public String getState() {
        return state.get();
    }

    public StringProperty stateProperty() {
        return state;
    }

    public void setState(String state) {
        this.state.set(state.toUpperCase());
    }

    public String getStationCallSign() {
        return stationCallSign.get();
    }

    public StringProperty stationCallSignProperty() {
        return stationCallSign;
    }

    public void setStationCallSign(String stationCallSign) {
        this.stationCallSign.set(stationCallSign.toUpperCase());
    }

    public String getsTx() {
        return sTx.get();
    }

    public StringProperty sTxProperty() {
        return sTx;
    }

    public void setsTx(String sTx) {
        this.sTx.set(sTx);
    }

    public String getsTxString() {
        return sTxString.get();
    }

    public StringProperty sTxStringProperty() {
        return sTxString;
    }

    public void setsTxString(String sTxString) {
        this.sTxString.set(sTxString);
    }

    public String getSwl() {
        return swl.get();
    }

    public StringProperty swlProperty() {
        return swl;
    }

    public void setSwl(String swl) {
        this.swl.set(swl);
    }

    public String getTenTen() {
        return tenTen.get();
    }

    public StringProperty tenTenProperty() {
        return tenTen;
    }

    public void setTenTen(String tenTen) {
        this.tenTen.set(tenTen.toUpperCase());
    }

    public LocalTime getTimeOff() {
        return timeOff.get();
    }

    public ObjectProperty<LocalTime> timeOffProperty() {
        return timeOff;
    }

    public void setTimeOff(LocalTime timeOff) {
        this.timeOff.set(timeOff);
    }

    public void setTimeOff(String timeOff) {
        this.timeOff.set(DateTimeWrapper.ofTime(timeOff));
    }

    public String getTxPwr() {
        return txPwr.get();
    }

    public StringProperty txPwrProperty() {
        return txPwr;
    }

    public void setTxPwr(String txPwr) {
        this.txPwr.set(txPwr);
    }

    public String getVeProv() {
        return veProv.get();
    }

    public StringProperty veProvProperty() {
        return veProv;
    }

    public void setVeProv(String veProv) {
        this.veProv.set(veProv);
    }

    public String getWeb() {
        return web.get();
    }

    public StringProperty webProperty() {
        return web;
    }

    public void setWeb(String web) {
        this.web.set(web);
    }

    public String getCustom1() {
        return custom1.get();
    }

    public StringProperty custom1Property() {
        return custom1;
    }

    public void setCustom1(String custom1) {
        this.custom1.set(custom1.toUpperCase());
    }

    public String getCustom2() {
        return custom2.get();
    }

    public StringProperty custom2Property() {
        return custom2;
    }

    public void setCustom2(String custom2) {
        this.custom2.set(custom2.toUpperCase());
    }

    public String getCustom3() {
        return custom3.get();
    }

    public StringProperty custom3Property() {
        return custom3;
    }

    public void setCustom3(String custom3) {
        this.custom3.set(custom3.toUpperCase());
    }

    public String getCustom4() {
        return custom4.get();
    }

    public StringProperty custom4Property() {
        return custom4;
    }

    public void setCustom4(String custom4) {
        this.custom4.set(custom4.toUpperCase());
    }

    public String getAppSwarlogTerritory() {
        return appSwarlogTerritory.get();
    }

    public StringProperty appSwarlogTerritoryProperty() {
        return appSwarlogTerritory;
    }

    public void setAppSwarlogTerritory(String appSwarlogTerritory) {
        this.appSwarlogTerritory.set(appSwarlogTerritory.toUpperCase());
    }

    public String getAppSwarlogTz() {
        return appSwarlogTz.get();
    }

    public StringProperty appSwarlogTzProperty() {
        return appSwarlogTz;
    }

    public void setAppSwarlogTz(String appSwarlogTz) {
        this.appSwarlogTz.set(appSwarlogTz.toUpperCase());
    }

}
