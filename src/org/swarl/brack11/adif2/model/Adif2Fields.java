/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.brack11.adif2.model;

/**
 * Class is the part of adif parser package
 * Created by Yury Bondarenko on 3/16/17.
 */
public enum Adif2Fields {
    ADDRESS,
    ADIF_VER,
    AGE,
    A_INDEX,
    ANT_AZ,
    ANT_EL,
    ANT_PATH,
    ARRL_SECT,
    BAND,
    BAND_RX,
    CALL,
    CHECK,
    CLASS,
    CNTY,
    COMMENT,
    CONT,
    CONTACTED_OP,
    CONTEST_ID,
    COUNTRY,
    CQZ,
    CREDIT_SUBMITTED,
    CREDIT_GRANTED,
    DISTANCE,
    DXCC,
    EMAIL,
    EQ_CALL,
    EQSL_QSLRDATE,
    EQSL_QSLSDATE,
    EQSL_QSL_RCVD,
    EQSL_QSL_SENT,
    FORCE_INIT,
    FREQ,
    FREQ_RX,
    GRIDSQUARE,
    GUEST_OP,
    IOTA,
    IOTA_ISLAND_ID,
    ITUZ,
    K_INDEX,
    LAT,
    LON,
    LOTW_QSLRDATE,
    LOTW_QSLSDATE,
    LOTW_QSL_RCVD,
    LOTW_QSL_SENT,
    MAX_BURSTS,
    MODE,
    MS_SHOWER,
    MY_CITY,
    MY_CNTY,
    MY_COUNTRY,
    MY_CQ_ZONE,
    MY_GRIDSQUARE,
    MY_IOTA,
    MY_IOTA_ISLAND_ID,
    MY_ITU_ZONE,
    MY_LAT,
    MY_LON,
    MY_NAME,
    MY_POSTAL_CODE,
    MY_RIG,
    MY_SIG,
    MY_SIG_INFO,
    MY_STATE,
    MY_STREET,
    NAME,
    NOTES,
    NR_BURSTS,
    NR_PINGS,
    OPERATOR,
    OWNER_CALLSIGN,
    PFX,
    PRECEDENCE,
    PROGRAMID,
    PROGRAMVERSION,
    PROP_MODE,
    PUBLIC_KEY,
    QSLMSG,
    QSLRDATE,
    QSLSDATE,
    QSL_RCVD,
    QSL_RCVD_VIA,
    QSL_SENT,
    QSL_SENT_VIA,
    QSL_VIA,
    QSO_COMPLETE,
    QSO_DATE,
    QSO_DATE_OFF,
    QSO_RANDOM,
    QTH,
    RIG,
    RST_RCVD,
    RST_SENT,
    RX_PWR,
    SAT_MODE,
    SAT_NAME,
    SFI,
    SIG,
    SIG_INFO,
    SRX,
    SRX_STRING,
    STATE,
    STATION_CALLSIGN,
    STX,
    STX_STRING,
    SWL,
    TEN_TEN,
    TIME_OFF,
    TIME_ON,
    TX_PWR,
    VE_PROV,
    WEB,
    APP_SWARLOG_TERRITORY,
    APP_SWARLOG_TZ;
}
