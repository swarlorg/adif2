/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.brack11.adif2.model;

import org.swarl.brack11.adif2.enums.Band;
import org.swarl.brack11.adif2.enums.Mode;
import org.swarl.brack11.adif2.enums.QSLRcvd;
import org.swarl.brack11.adif2.utils.DateTimeWrapper;
import org.swarl.brack11.location.model.Coordinate;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

/**
 * Created by Yury Bondarenko on 2017-04-17.
 * <p>
 * Owner: Yury Bondarenko
 * Website: http://www.swarl.org
 * Email: this.brack@gmail.com
 */
public class Adif2Wrapper {
    private Optional<String> call;
    private Optional<String> comment;
    private Optional<Band> band;
    private Optional<LocalDate> qsoDate;
    private Optional<Double> freq;
    private Optional<Mode> mode;
    private Optional<String> rstRcvd;
    private Optional<String> rstSent;
    private Optional<LocalTime> timeOn;
    private Optional<Band> bandRx;
    private Optional<String> cnty;
    private Optional<String> cont;
    private Optional<String> country;
    private Optional<String> cqz;
    private Optional<String> distance;
    private Optional<String> dxcc;
    private Optional<LocalDate> eQslQslRDate;
    private Optional<LocalDate> eQslQslSDate;
    private Optional<QSLRcvd> eQslQslRcvd;
    private Optional<String> eQslQslSent;
    private Optional<String> gridSquare;
    private Optional<String> iota;
    private Optional<String> ituz;
    private Optional<Coordinate> lat;
    private Optional<Coordinate> lon;
    private Optional<String> myCity;
    private Optional<String> myCnty;
    private Optional<String> myCountry;
    private Optional<String> myCqZone;
    private Optional<String> myGridSquare;
    private Optional<String> myIota;
    private Optional<String> myItuZone;
    private Optional<Coordinate> myLat;
    private Optional<Coordinate> myLon;
    private Optional<String> myName;
    private Optional<String> notes;
    private Optional<String> pfx;
    private Optional<String> programVersion;
    private Optional<String> qslmsg;
    private Optional<LocalDate> qslRDate;
    private Optional<LocalDate> qslSDate;
    private Optional<QSLRcvd> qslRcvd;
    private Optional<String> qslRcvdVia;
    private Optional<String> qslSent;
    private Optional<String> qslSentVia;
    private Optional<String> qslVia;
    private Optional<String> swl;
    private Optional<String> appSwarlogTerritory;
    private Optional<String> appSwarlogTz;

    public Adif2Wrapper(Adif2Record record) {
        call = Optional.ofNullable(record.getCall());
        comment = Optional.ofNullable(record.getComment());
        band = Optional.ofNullable(record.getBand());
        qsoDate = Optional.ofNullable(record.getQsoDate());
        freq = Optional.ofNullable(record.getFreq());
        mode = Optional.ofNullable(record.getMode());
        rstRcvd = Optional.ofNullable(record.getRstRcvd());
        rstSent = Optional.ofNullable(record.getRstSent());
        timeOn = Optional.ofNullable(record.getTimeOn());
        bandRx = Optional.ofNullable(record.getBandRx());
        cnty = Optional.ofNullable(record.getCnty());
        cont = Optional.ofNullable(record.getCont());
        country = Optional.ofNullable(record.getCountry());
        cqz = Optional.ofNullable(record.getCqz());
        distance = Optional.ofNullable(record.getDistance());
        dxcc = Optional.ofNullable(record.getDxcc());
        eQslQslRDate = Optional.ofNullable(record.getEqslQslRDate());
        eQslQslSDate = Optional.ofNullable(record.getEqslQslSDate());
        eQslQslRcvd = Optional.ofNullable(record.getEqslQslRcvd());
        eQslQslSent = Optional.ofNullable(record.getEqslQslSent());
        gridSquare = Optional.ofNullable(record.getGridsquare());
        ituz = Optional.ofNullable(record.getItuz());
        iota = Optional.ofNullable(record.getIota());
        lat = Optional.ofNullable(record.getLat());
        lon = Optional.ofNullable(record.getLon());
        myCity = Optional.ofNullable(record.getMyCity());
        myCnty = Optional.ofNullable(record.getMyCnty());
        myCountry = Optional.ofNullable(record.getMyCountry());
        myCqZone = Optional.ofNullable(record.getMyCqZone());
        myGridSquare = Optional.ofNullable(record.getMyGridSquare());
        myIota = Optional.ofNullable(record.getMyIota());
        myItuZone = Optional.ofNullable(record.getMyItuZone());
        myLat = Optional.ofNullable(record.getMyLat());
        myLon = Optional.ofNullable(record.getMyLon());
        myName = Optional.ofNullable(record.getMyName());
        notes = Optional.ofNullable(record.getNotes());
        pfx = Optional.ofNullable(record.getPfx());
        programVersion = Optional.ofNullable(record.getProgramVersion());
        qslmsg = Optional.ofNullable(record.getQslmsg());
        qslRDate = Optional.ofNullable(record.getQslRDate());
        qslSDate = Optional.ofNullable(record.getQslSDate());
        qslRcvd = Optional.ofNullable(record.getQslRcvd());
        qslRcvdVia = Optional.ofNullable(record.getQslRcvdVia());
        qslSent = Optional.ofNullable(record.getQslSent());
        qslSentVia = Optional.ofNullable(record.getQslSentVia());
        qslVia = Optional.ofNullable(record.getQslVia());
        swl = Optional.ofNullable(record.getSwl());
        appSwarlogTerritory = Optional.ofNullable(record.getAppSwarlogTerritory());
        appSwarlogTz = Optional.ofNullable(record.getAppSwarlogTz());
    }

    public Adif2Wrapper() {

    }

    public String buildHeader() {
        StringBuilder sb = new StringBuilder();
        sb.append("ADIF 2 Export from SWARLOG\n");
        sb.append("Generated on " + LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE) + " " + LocalTime.now().format(DateTimeFormatter.ISO_TIME) + "\n");
        sb.append("<PROGRAMID:7>SWARLOG\n");
        sb.append("<ADIF_Ver:1>2\n");
        sb.append("<EOH>\n");

        return sb.toString();
    }

    @Override
    public String toString() {
            StringBuilder str = new StringBuilder();

            str.append("<" + Adif2Fields.CALL + ":" + call.get().length() + ">" + call.get());
            if (comment.isPresent()) str.append("<" + Adif2Fields.COMMENT + ":" + comment.get().length() + ">" + comment.get());
            if (band.isPresent()) str.append("<" + Adif2Fields.BAND + ":" + band.get().length() + ">" + band.get().toString());
            if (qsoDate.isPresent()) str.append("<" + Adif2Fields.QSO_DATE + ":8:D>" + DateTimeWrapper.toAdifDate(qsoDate.get()));
            if (freq.isPresent()) str.append("<" + Adif2Fields.FREQ + ":" + freq.get().toString().length() + ">" + freq.get().toString());
            if (mode.isPresent()) str.append("<" + Adif2Fields.MODE + ":" + mode.get().toString().length() + ">" + mode.get().toString());
            if (rstRcvd.isPresent()) str.append("<" + Adif2Fields.RST_RCVD + ":" + rstRcvd.get().length() + ">" + rstRcvd.get());
            if (rstSent.isPresent()) str.append("<" + Adif2Fields.RST_SENT + ":" + rstSent.get().length() + ">" + rstSent.get());
            if (timeOn.isPresent()) str.append("<" + Adif2Fields.TIME_ON + ":4>" + DateTimeWrapper.toAdifTime(timeOn.get()));
            if (bandRx.isPresent()) str.append("<" + Adif2Fields.BAND_RX + ":" + bandRx.get().length() + ">" + bandRx.get().toString());
            if (cnty.isPresent()) str.append("<" + Adif2Fields.CNTY + ":" + cnty.get().length() + ">" + cnty.get());
            if (country.isPresent()) str.append("<" + Adif2Fields.COUNTRY + ":" + country.get().length() + ">" + country.get());
            if (cont.isPresent()) str.append("<" + Adif2Fields.CONT + ":" + cont.get().length() + ">" + cont.get());
            if (cqz.isPresent()) str.append("<" + Adif2Fields.CQZ + ":" + cqz.get().length() + ">" + cqz.get());
            if (distance.isPresent()) str.append("<" + Adif2Fields.DISTANCE + ":" + distance.get().length() + ">" + distance.get());
            if (dxcc.isPresent()) str.append("<" + Adif2Fields.DXCC + ":" + dxcc.get().length() + ">" + dxcc.get());
            if (eQslQslRDate.isPresent()) str.append("<" + Adif2Fields.EQSL_QSLRDATE + ":8:D>" + DateTimeWrapper.toAdifDate(eQslQslRDate.get()));
            if (eQslQslSDate.isPresent()) str.append("<" + Adif2Fields.EQSL_QSLSDATE + ":8:D>" + DateTimeWrapper.toAdifDate(eQslQslSDate.get()));
            if (eQslQslRcvd.isPresent()) str.append("<" + Adif2Fields.EQSL_QSL_RCVD + ":1>" + eQslQslRcvd.get().getCode());
            if (eQslQslSent.isPresent()) str.append("<" + Adif2Fields.EQSL_QSL_SENT + ":" + eQslQslSent.get().length() + ">" + eQslQslSent.get());
            if (gridSquare.isPresent()) str.append("<" + Adif2Fields.GRIDSQUARE + ":" + gridSquare.get().length() + ">" + gridSquare.get());
            if (ituz.isPresent()) str.append("<" + Adif2Fields.ITUZ + ":" + ituz.get().length() + ">" + ituz.get());
            if (iota.isPresent()) str.append("<" + Adif2Fields.IOTA + ":" + iota.get().length() + ">" + iota.get());
            if (lat.isPresent()) str.append("<" + Adif2Fields.LAT + ":" + lat.get().getCoordinate().length() + ">" + lat.get().getCoordinate());
            if (lon.isPresent()) str.append("<" + Adif2Fields.LON + ":" + lon.get().getCoordinate().length() + ">" + lon.get().getCoordinate());
            if (myCity.isPresent()) str.append("<" + Adif2Fields.MY_CITY + ":" + myCity.get().length() + ">" + myCity.get());
            if (myCnty.isPresent()) str.append("<" + Adif2Fields.MY_CNTY + ":" + myCnty.get().length() + ">" + myCnty.get());
            if (myCountry.isPresent()) str.append("<" + Adif2Fields.MY_COUNTRY + ":" + myCountry.get().length() + ">" + myCountry.get());
            if (myCqZone.isPresent()) str.append("<" + Adif2Fields.MY_CQ_ZONE + ":" + myCqZone.get().length() + ">" + myCqZone.get());
            if (myGridSquare.isPresent()) str.append("<" + Adif2Fields.MY_GRIDSQUARE + ":" + myGridSquare.get().length() + ">" + myGridSquare.get());
            if (myIota.isPresent()) str.append("<" + Adif2Fields.MY_IOTA + ":" + myIota.get().length() + ">" + myIota.get());
            if (myItuZone.isPresent()) str.append("<" + Adif2Fields.MY_ITU_ZONE + ":" + myItuZone.get().length() + ">" + myItuZone.get());
            if (myLat.isPresent()) str.append("<" + Adif2Fields.MY_LAT + ":" + myLat.get().getCoordinate().length() + ">" + myLat.get().getCoordinate());
            if (myLon.isPresent()) str.append("<" + Adif2Fields.MY_LON + ":" + myLon.get().getCoordinate().length() + ">" + myLon.get().getCoordinate());
            if (myName.isPresent()) str.append("<" + Adif2Fields.MY_NAME + ":" + myName.get().length() + ">" + myName.get());
            if (notes.isPresent()) str.append("<" + Adif2Fields.NOTES + ":" + notes.get().length() + ">" + notes.get());
            if (pfx.isPresent()) str.append("<" + Adif2Fields.PFX + ":" + pfx.get().length() + ">" + pfx.get());
            if (programVersion.isPresent()) str.append("<" + Adif2Fields.PROGRAMVERSION + ":" + programVersion.get().length() + ">" + programVersion.get());
            if (qslmsg.isPresent()) str.append("<" + Adif2Fields.QSLMSG + ":" + qslmsg.get().length() + ">" + qslmsg.get());
            if (qslRDate.isPresent()) str.append("<" + Adif2Fields.QSLRDATE + ":8:D>" + DateTimeWrapper.toAdifDate(qslRDate.get()));
            if (qslSDate.isPresent()) str.append("<" + Adif2Fields.QSLSDATE + ":8:D>" + DateTimeWrapper.toAdifDate(qslSDate.get()));
            if (qslRcvd.isPresent()) str.append("<" + Adif2Fields.QSL_RCVD + ":1>" + qslRcvd.get().getCode());
            if (qslRcvdVia.isPresent()) str.append("<" + Adif2Fields.QSL_RCVD_VIA + ":" + qslRcvdVia.get().length() + ">" + qslRcvdVia.get());
            if (qslSent.isPresent()) str.append("<" + Adif2Fields.QSL_SENT + ":" + qslSent.get().length() + ">" + qslSent.get());
            if (qslSentVia.isPresent()) str.append("<" + Adif2Fields.QSL_SENT_VIA + ":" + qslSentVia.get().length() + ">" + qslSentVia.get());
            if (qslVia.isPresent()) str.append("<" + Adif2Fields.QSL_VIA + ":" + qslVia.get().length() + ">" + qslVia.get());
            if (swl.isPresent()) str.append("<" + Adif2Fields.SWL + ":" + swl.get().length() + ">" + swl.get());
            if (appSwarlogTerritory.isPresent()) str.append("<" + Adif2Fields.APP_SWARLOG_TERRITORY + ":" + appSwarlogTerritory.get().length() + ">" + appSwarlogTerritory.get());
            if (appSwarlogTz.isPresent()) str.append("<" + Adif2Fields.APP_SWARLOG_TZ + ":" + appSwarlogTz.get().length() + ">" + appSwarlogTz.get());
            str.append("<EOR>\n");

        return str.toString();
    }

}
