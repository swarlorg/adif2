/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.brack11.adif2;

import org.swarl.brack11.adif2.model.Adif2Record;
import org.swarl.brack11.adif2.model.Adif2Wrapper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Yury Bondarenko on 2017-03-31.
 * <p>
 * Owner: Yury Bondarenko
 * Website: http://www.swarl.org
 * Email: this.brack@gmail.com
 */
public class Adif2Writer {

    public Adif2Writer() {
    }

    public void write(Adif2Record record, String filePath) {
        Adif2Wrapper wrapper = new Adif2Wrapper(record);

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filePath, true))){
            bw.write(wrapper.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void newFile(File filePath) {
        Adif2Wrapper wrapper = new Adif2Wrapper();
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filePath, false))) {
            bw.write(wrapper.buildHeader());
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
