/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.brack11.adif2.enums;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Yury Bondarenko on 2017-04-10.
 * <p>
 * Owner: Yury Bondarenko
 * Website: http://www.swarl.org
 * Email: this.brack@gmail.com
 */
public enum Mode {

    AM(null,"AM"),
    ARDOP(null,"ARDOP"),
    ATV(null,"ATV"),
    CLO(null,"CLO"),
    CONTESTI(null,"CONTESTI"),
    DIGITALVOICE(null,"DIGITALVOICE"),
    DSTAR(null,"DSTAR"),
    FAX(null,"FAX"),
    FM(null,"FM"),
    FSK441(null,"FSK441"),
    JT6M(null,"JT6M"),
    JT44(null,"JT44"),
    MSK144(null,"MSK144"),
    MT63(null,"MT63"),
    PKT(null,"PKT"),
    PSK2K(null,"PSK2K"),
    Q15(null,"Q15"),
    RTTYM(null,"RTTYM"),
    SSTV(null,"SSTV"),
    THOR(null,"THOR"),
    V4(null,"V4"),
    VOI(null,"VOI"),
    WINMOR(null,"WINMOR"),
    WSPR(null,"WSPR"),

    CHIP(null,"CHIP"),
        CHIP64(CHIP, "CHIP64"),
        CHIP128(CHIP, "CHIP128"),
    CW(null,"CW"),
        PCW(CW, "PCW"),
    DOMINO(null,"DOMINO"),
        DOMINOEX(DOMINO, "DOMINOEX"),
        DOMINOF(DOMINO, "DOMINOF"),
    HELL(null,"HELL"),
        FMHELL(HELL, "FMHELL"),
        FSKHELL(HELL, "FSKHELL"),
        HELL80(HELL, "HELL80"),
        HFSK(HELL, "HFSK"),
        PSKHELL(HELL, "PSKHELL"),
    ISCAT(null,"ISCAT"),
        ISCAT_A(ISCAT, "ISCAT-A"),
        ISCAT_B(ISCAT, "ISCAT-B"),
    JT4(null,"JT4"),
        JT4A(JT4, "JT4A"),
        JT4B(JT4, "JT4B"),
        JT4C(JT4, "JT4C"),
        JT4D(JT4, "JT4D"),
        JT4E(JT4, "JT4E"),
        JT4F(JT4, "JT4F"),
        JT4G(JT4, "JT4G"),
    JT9(null,"JT9"),
        JT9_1(JT9, "JT9-1"),
        JT9_2(JT9, "JT9-2"),
        JT9_5(JT9, "JT9-5"),
        JT9_10(JT9, "JT9-10"),
        JT9_30(JT9, "JT9-30"),
    JT65(null,"JT65"),
        JT65A(JT65, "JT65A"),
        JT65B(JT65, "JT65B"),
        JT65B2(JT65, "JT65B2"),
        JT65C(JT65, "JT65C"),
        JT65C2(JT65, "JT65C2"),
    MFSK(null,"MFSK"),
        MFSK4(MFSK, "MFSK4"),
        MFSK8(MFSK, "MFSK8"),
        MFSK11(MFSK, "MFSK11"),
        MFSK16(MFSK, "MFSK16"),
        MFSK22(MFSK, "MFSK22"),
        MFSK31(MFSK, "MFSK31"),
        MFSK32(MFSK, "MFSK32"),
        MFSK64(MFSK, "MFSK64"),
        MFSK128(MFSK, "MFSK128"),
    OLIVIA(null,"OLIVIA"),
        OLIVIA_4_125(OLIVIA, "OLIVIA 4/125"),
        OLIVIA_4_250(OLIVIA, "OLIVIA 4/250"),
        OLIVIA_8_250(OLIVIA, "OLIVIA 8/250"),
        OLIVIA_8_500(OLIVIA, "OLIVIA 8/500"),
        OLIVIA_16_500(OLIVIA, "OLIVIA 16/500"),
        OLIVIA_16_1000(OLIVIA, "OLIVIA 16/1000"),
        OLIVIA_32_1000(OLIVIA, "OLIVIA 32/1000"),
    OPERA(null,"OPERA"),
        OPERA_BEACON(OPERA, "OPERA-BEACON"),
        OPERA_QSO(OPERA, "OPERA-QSO"),
    PAC(null,"PAC"),
        PAC2(PAC, "PAC2"),
        PAC3(PAC, "PAC3"),
        PAC4(PAC, "PAC4"),
    PAX(null,"PAX"),
        PAX2(PAX, "PAX2"),
    PSK(null,"PSK"),
        FSK31(PSK, "FSK31"),
        PSK10(PSK, "PSK10"),
        PSK31(PSK, "PSK31"),
        PSK63(PSK, "PSK63"),
        PSK63F(PSK, "PSK63F"),
        PSK125(PSK, "PSK125"),
        PSK250(PSK, "PSK250"),
        PSK500(PSK, "PSK500"),
        PSK1000(PSK, "PSK1000"),
        PSKAM10(PSK, "PSKAM10"),
        PSKAM31(PSK, "PSKAM31"),
        PSKAM50(PSK, "PSKAM50"),
        PSKFEC31(PSK, "PSKFEC31"),
        QPSK31(PSK, "QPSK31"),
        QPSK63(PSK, "QPSK63"),
        QPSK125(PSK, "QPSK125"),
        QPSK250(PSK, "QPSK250"),
        QPSK500(PSK, "QPSK500"),
        SIM31(PSK, "SIM31"),
    QRA64(null,"QRA64"),
        QRA64A(QRA64, "QRA64A"),
        QRA64B(QRA64, "QRA64B"),
        QRA64C(QRA64, "QRA64C"),
        QRA64D(QRA64, "QRA64D"),
        QRA64E(QRA64, "QRA64E"),
    ROS(null,"ROS"),
        ROS_EME(ROS, "ROS-EME"),
        ROS_HF(ROS, "ROS-HF"),
        ROS_MF(ROS, "ROS-MF"),
    RTTY(null,"RTTY"),
        ASCI(RTTY, "ASCI"),
    SSB(null,"SSB"),
        LSB(SSB, "LSB"),
        USB(SSB, "USB"),
    THRB(null,"THRB"),
        THRBX(THRB, "THRBX"),
    TOR(null,"TOR"),
        AMTORFEC(TOR, "AMTORFEC"),
        GTOR(TOR, "GTOR");

    private Mode parent;
    private String name;
    private List<Mode> children = new ArrayList<Mode>();

    private static Comparator<Mode> compareNames = new Comparator<Mode>() {
        @Override
        public int compare(Mode m1, Mode m2) {
            String modeName1 = m1.name.toUpperCase();
            String modeName2 = m2.name.toUpperCase();
            return modeName1.compareTo(modeName2);
        }
    };

    Mode(Mode parent, String name) {
        this.parent = parent;
        this.name = name;
        if (this.parent != null) {
            this.parent.addChild(this);
        }
    }

    private void addChild(Mode mode) {
        this.children.add(mode);
    }


    private void addChildren(Mode mode, List<Mode> list) {
        list.addAll(mode.children);
        for (Mode child : mode.children) {
            addChildren(child, list);
        }
    }

    public static Mode[] childrenOf(Mode p) {
        if (p.hasChildren()) {
            List<Mode> children = new ArrayList<>();
            for (Mode child : values()) {
                if (child.parent == p) {
                    children.add(child);
                }
            }
            children.sort(compareNames);
            return children.toArray(new Mode[children.size()]);
        }
        return new Mode[]{p};
    }

    public static boolean isRoot(Mode m) {
        if (m.parent == null) {
            return true;
        }
        return false;
    }

    public static Mode[] roots() {
        List<Mode> roots = new ArrayList<>();
        for (Mode mode : values()) {
            if (mode.parent == null) {
                roots.add(mode);
            }
        }
        roots.sort(compareNames);
        return roots.toArray(new Mode[roots.size()]);
    }

    public boolean is(Mode other) {
        if (other == null) {
            return false;
        }

        for (Mode mode = this;  mode != null;  mode = mode.parent()) {
            if (other == mode) {
                return true;
            }
        }
        return false;
    }

    public boolean hasChildren() {
        for (Mode mode : values()) {
            if (mode.parent == this) {
                return true;
            }
        }
        return false;
    }

    public Mode[] children() {
        return children.toArray(new Mode[children.size()]);
    }

    public Mode[] allChildren() {
        List<Mode> list = new ArrayList<Mode>();
        addChildren(this, list);
        return list.toArray(new Mode[list.size()]);
    }

    public Mode parent() {
        return parent;
    }


    public static Mode getByName(String name) {
        for (Mode mode : values()) {
            if (mode.toString().equalsIgnoreCase(name)) {
                return mode;
            }
        }
        return null;
    }

    public String toString() {
        return name;
    }

}
