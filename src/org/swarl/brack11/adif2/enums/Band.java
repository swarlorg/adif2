/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.brack11.adif2.enums;

/**
 * Created by Yury Bondarenko on 2017-04-09.
 * <p>
 * Owner: Yury Bondarenko
 * Website: http://www.swarl.org
 * Email: this.brack@gmail.com
 */
public enum Band {
    BAND2190m("2190m",0.1357,0.1378),
    BAND630m("630m",0.472,0.479),
    BAND560m("560m",0.501,0.504),
    BAND160m("160m",1.8,2.0),
    BAND80m("80m",3.5,4.0),
    BAND60m("60m",5.06,5.45),
    BAND40m("40m",7.0,7.3),
    BAND30m("30m",10.1,10.15),
    BAND20m("20m",14.0,14.35),
    BAND17m("17m",18.068,18.168),
    BAND15m("15m",21.0,21.45),
    BAND12m("12m",24.890,24.99),
    BAND10m("10m",28.0,29.7),
    BAND6m("6m",50,54),
    BAND4m("4m",70,71),
    BAND2m("2m",144,148),
    BAND1_25m("1.25m",222,225),
    BAND70cm("70cm",420,450),
    BAND33cm("30cm",902,928),
    BAND23cm("23cm",1240,1300),
    BAND13cm("13cm",2300,2450),
    BAND9cm("9cm",3300,3500),
    BAND6cm("6cm",5650,5925),
    BAND3cm("3cm",10000,10500),
    BAND1_25cm("1.25cm",24000,24250),
    BAND6mm("6mm",47000,47200),
    BAND4mm("4mm",75500,81000),
    BAND2_5mm("2.5mm",119980,120020),
    BAND2mm("2mm",142000,149000),
    BAND1mm("1mm",241000,250000);

    private final double min;
    private final double max;
    private final String name;

    Band(String name, double min, double max) {
        this.min = min;
        this.max = max;
        this.name = name;
    }


    public String toString() {
        return name;
    }


    public int length() {
        return name.length();
    }


    public static Band getByName(String name) {
        for (Band band : values()) {
            if (band.toString().equalsIgnoreCase(name)) {
                return band;
            }
        }
        return null;
    }


    public static Band getByFreq(double freq) {
        //Double mhz = freq / 1000;
        Double mhz = kHzTomHz(freq);
        if (mhz != 0) {
            for (Band band : values()) {
                if ((mhz >= band.min) && (mhz <= band.max)) {
                    return band;
                }
            }
        }
        return null;
    }

    public static double kHzTomHz(double freq) {
        double wrong = 0;
        for (Band band : values()) {
            if ((freq >= band.min) && (freq <= band.max)) {
                return freq;
            }
            double mhz = freq / 1000;
            if ((mhz >= band.min) && (mhz <= band.max)) {
                return mhz;
            }
        }
        return wrong;
    }
}
