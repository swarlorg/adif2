# README #

### What is this repository for? ###

Java 1.8 based project designed for parsing ADIF2 format files described in http://www.adif.org

### How do I get set up? ###

call constructor of Adift2Reader class with parameters filePath - path to adi file, and ObservableList<Adif2Record> records - list of records where parsed data will be stored

        
```
#!java

try {
   Adif2Reader adifReader = new Adif2Reader("pathto/filename.adi",records);

   adifReader.read(); // actual parsing
   records = adifReader.getRecords(); // get parsed ObservableList to use in javaFX tableview

} catch (IOException e) {
   e.printStackTrace();
}
```

Writing data into adif:

```
Adif2Record record = new Adif2Record();
Adif2Writer writer = new Adif2Writer();
String filePath = "/home/user/file.adi";

record.setCall("CA6LL");
record.setQsoDate("20170405");
...
write(record, filePath);

```

### Contribution guidelines ###

Code is licensed under Apache license version 2 http://www.apache.org/licenses/LICENSE-2.0
Code can be freely used by any project

### Who do I talk to? ###

* Owner Yury Bondarenko
* Email this.brack@gmail.com